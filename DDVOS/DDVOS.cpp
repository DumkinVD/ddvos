#include "stdafx.h"
#include "Resource.h"
#include <fstream>
#include <ctime>

#pragma comment(lib, "liblua53.a")

#include <Lua\include\lua.hpp>
#include <LuaBridge\LuaBridge.h>
#include <tchar.h>
#include <vector>
#include <string>
#include <codecvt>
#include <thread>

using namespace luabridge;
using namespace std;

#define MAX_LOADSTRING 100

struct INTPOINT {
	int x;
	int y;
};
struct GPUPoint {
	INTPOINT point;
	COLORREF color;
};
vector<GPUPoint> gpu_task_point;

struct GPULine {
	INTPOINT pointFirst;
	INTPOINT pointSecond;
	COLORREF color;
};
vector<GPULine> gpu_task_line;

struct GPUFONT {
	int size;
	COLORREF color;
	HFONT hFont;
};
struct GPUText {
	INTPOINT position;
	wstring text;
	GPUFONT font;
};
vector<GPUText> gpu_task_text;

struct CARET {
	COLORREF color;
	unsigned int position;
};
struct InputText {
	wstring text;
	RECT rect;
	unsigned int max_lenght;
	bool isStarted = false;
	GPUFONT font;
	CARET caret;
};

InputText Text;
HWND hWnd;
HINSTANCE hInst;
WCHAR szTitle[MAX_LOADSTRING];
WCHAR szWindowClass[MAX_LOADSTRING];
lua_State * L[100];
bool L_s[100];
wstring FILE_PATH;

ATOM MyRegisterClass(HINSTANCE hInstance);
BOOL InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK About(HWND, UINT, WPARAM, LPARAM);

wstring _stdcall AnsiStringToWide(string const &Str, UINT CodePage = CP_ACP)
{
	DWORD const BuffSize = MultiByteToWideChar(CodePage, 0, Str.c_str(), -1, NULL, 0);
	if (!BuffSize) return NULL;
	vector<wchar_t> Buffer;
	Buffer.resize(BuffSize);
	if (!MultiByteToWideChar(CodePage, 0, Str.c_str(), -1, &Buffer[0], BuffSize)) return NULL;
	return (&Buffer[0]);
}
string _stdcall WideStringToAnsi(wstring const &Str, UINT CodePage = CP_ACP)
{
	DWORD const BuffSize = WideCharToMultiByte(CodePage, 0, Str.c_str(), -1, NULL, 0, NULL, NULL);
	if (!BuffSize) return NULL;
	vector<char> Buffer;
	Buffer.resize(BuffSize);
	if (!WideCharToMultiByte(CodePage, 0, Str.c_str(), -1, &Buffer[0], BuffSize, NULL, NULL)) return NULL;
	return (&Buffer[0]);
}

SIZE getSizeText(wstring text, int font_size) {
	HDC hdc;
	HFONT hFont = CreateFont(font_size, 0, 0, 0, 0, 0, 0, 0, DEFAULT_CHARSET, 0, 0, 0, 0, L"Arial Bold");

	SelectObject(hdc, hFont);
	SIZE size;
	HWND hwndDesktop = GetDesktopWindow();
	HDC hdcDesktop = GetDC(hwndDesktop);
	SetMapMode(hdcDesktop, MM_TEXT);
	SelectObject(hdcDesktop, hFont);

	GetTextExtentPoint32(hdcDesktop, (LPCWSTR)text.c_str(), text.length(), &size);

	return size;
}
int WText(int lenght, wstring text, int font_size) {
	wstring st;
	st = text.substr(0, lenght);
	SIZE size = getSizeText(st, font_size);
	
	return size.cx;
}

void Lua_GPU_DrawPixel(int x, int y, int color) {
	GPUPoint Pixel;

	Pixel.point.x	= x;
	Pixel.point.y	= y;
	Pixel.color		= (COLORREF) color;

	gpu_task_point.push_back(Pixel);
	InvalidateRect(hWnd, NULL, false);
}
void Lua_GPU_DrawLine(int xFirst, int yFirst, int xSecond, int ySecond, int color) {
	GPULine Line;

	Line.pointFirst.x	= xFirst;
	Line.pointFirst.y	= yFirst;
	Line.pointSecond.x	= xSecond;
	Line.pointSecond.y	= ySecond;
	Line.color			= (COLORREF) color;

	gpu_task_line.push_back(Line);
	InvalidateRect(hWnd, NULL, false);
}
void Lua_GPU_DrawText(int x, int y, string string, int size, int color) {
	GPUText Text;

	Text.position.x	= x;
	Text.position.y = y;
	Text.text		= AnsiStringToWide(string);
	Text.font.size	= size;
	Text.font.color = (COLORREF) color;
	Text.font.hFont	= CreateFont(Text.font.size, 0, 0, 0, 0, 0, 0, 0, DEFAULT_CHARSET, 0, 0, 0, 0, L"Arial Bold");

	gpu_task_text.push_back(Text);
	InvalidateRect(hWnd, NULL, false);
}

void Log(string text) {
	time_t seconds = time(NULL) + 14400;
	tm *timeinfo = localtime(&seconds);

	wstring time = L"[" + to_wstring(timeinfo->tm_mday) + L"." + to_wstring(timeinfo->tm_mon + 1) + L"." + to_wstring(timeinfo->tm_year + 1900) + L"] [" +
		to_wstring(timeinfo->tm_hour) + L":" + to_wstring(timeinfo->tm_min) + L":" + to_wstring(timeinfo->tm_sec) + L"] ";

	std::wofstream LogFile(FILE_PATH + L"\\Log.txt", ios::app);
	LogFile << time << AnsiStringToWide(text) << endl;
	LogFile.close();
}

string Lua_Input_Read(int xFirst, int yFirst, int xSecond, int ySecond, int max_lenght, int size, int color) {
	Text.text			= L"";
	Text.max_lenght		= max_lenght;
	Text.rect.left		= xFirst;
	Text.rect.top		= yFirst;
	Text.rect.right		= xSecond;
	Text.rect.bottom	= ySecond;
	Text.font.size		= size;
	Text.font.color		= color;
	Text.caret.color	= (COLORREF) 0x00000000;
	Text.caret.position = 0;
	Text.font.hFont		= CreateFont(Text.font.size, 0, 0, 0, 0, 0, 0, 0, DEFAULT_CHARSET, 0, 0, 0, 0, L"Arial Bold");
	Text.isStarted		= true;

	CreateCaret(hWnd, (HBITMAP) NULL, 1, Text.font.size);
	SetCaretPos(Text.caret.position, Text.rect.top);
	ShowCaret(hWnd);
	
	while (Text.isStarted) {
		Sleep(1);
	}

	return WideStringToAnsi(Text.text);
}

string Lua_Stop(int PID) {
	if (PID < 100 && L_s[PID] == true) {
		lua_error(L[PID]);
		return "true";
	}
	return "false";
}
void Lua_Open(string file) {
	for (size_t i = 0; i < 100; i++) {
		if (L_s[i] == false) {
			L_s[i] = true;

			Log("file='" + file + "' i=" + to_string(i) + " start");

			L[i] = luaL_newstate();
			luaL_openlibs(L[i]);

			setGlobal(L[i], i, "pid");

			getGlobalNamespace(L[i]).
				beginNamespace("gpu").
					addFunction("drawPixel", Lua_GPU_DrawPixel).
					addFunction("drawLine", Lua_GPU_DrawLine).
					addFunction("drawText", Lua_GPU_DrawText).
				endNamespace();
			getGlobalNamespace(L[i]).
				beginNamespace("input").
					addFunction("read", Lua_Input_Read).
				endNamespace();
			getGlobalNamespace(L[i]).
				beginNamespace("file").
					addFunction("open", Lua_Open).
					addFunction("stop", Lua_Stop).
				endNamespace();

			luaL_dofile(L[i], /*w*/file.c_str());

			lua_pcall(L[i], 0, 0, 0);

			lua_close(L[i]);

			Log("file='" + file + "' i=" + to_string(i) + " stop");

			L_s[i] = false;
			break;
		}
	}

	//LuaRef s = getGlobal(L, "testString");
	//LuaRef n = getGlobal(L, "number");
	//"EntryPoint"

	//string luaString = s.cast<string>();
	//cout << luaString << endl;

	//int answer = n.cast<int>();
	//cout << "And here's our number:" << answer << endl;

	//LuaRef sumNumbers = getGlobal(L, "sumNumbers");
	//int result = sumNumbers(5, 4);
	//Lua_GPU_DrawText(0,0, to_string(result),50,0);

	//cout << "Result:" << result << endl;

	return;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow) {
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

	TCHAR buffer[MAX_PATH];
	GetCurrentDirectory(sizeof(buffer), buffer);

	FILE_PATH.insert(0, buffer);

	std::wofstream LogFile(FILE_PATH + L"\\Log.txt", ios::app);
	LogFile << "------------- START -------------" << endl;
	LogFile.close();

	for (size_t i = 0; i < 100; i++) {
		L_s[i] = false;
	}
	thread Thread_Lua_Boot(Lua_Open, "Boot/Init.lua");
	Thread_Lua_Boot.detach();

    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_DDVOS, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    if (!InitInstance (hInstance, nCmdShow)) {
        return false;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_DDVOS));
    MSG msg;

	while (GetMessage(&msg, nullptr, 0, 0)) {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}
ATOM MyRegisterClass(HINSTANCE hInstance) {
    WNDCLASSEXW wcex;

    wcex.cbSize			= sizeof(WNDCLASSEX);
    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE (IDI_DDVOS));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH) (COLOR_WINDOW + 1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_DDVOS);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE (IDI_SMALL));

    return RegisterClassExW(&wcex);
}
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow) {
   hInst = hInstance;

   hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd) {
      return false;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return true;
}
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    switch (message) {
	case WM_INITDIALOG: {
		break;
	}
	case WM_CREATE: {
		//MessageBox(NULL, L"test0.11", L"DDVOS", MB_ICONASTERISK | MB_OK);
		break;
	}
	case WM_COMMAND: {
		int wmId = LOWORD(wParam);

		switch (wmId) {
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}

		break;
	}
	case WM_LBUTTONDOWN: {
		break;
	}
	case WM_LBUTTONUP: {
		break;
	}
	case WM_MOUSEMOVE: {
		POINT point;
		GetCursorPos(&point);
		ScreenToClient(hWnd, &point);
		break;
	}
	case WM_SETFOCUS: {
		if (Text.isStarted) {
			CreateCaret(hWnd, (HBITMAP)NULL, 1, Text.font.size);
			SetCaretPos(WText(Text.caret.position, Text.text, Text.font.size) + Text.rect.left, Text.rect.top);
			ShowCaret(hWnd);
		}
		break;
	}
	case WM_DESTROY: {
		PostQuitMessage(0);
		break;
	}
	case WM_KILLFOCUS: {
		if (Text.isStarted) {
			HideCaret(hWnd);
			DestroyCaret();
		}
		break;
	}
    case WM_PAINT: {
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);

		while (gpu_task_point.size()) {
			SetPixel(hdc, gpu_task_point[0].point.x, gpu_task_point[0].point.y, gpu_task_point[0].color);

			gpu_task_point.erase(gpu_task_point.begin());
		}

		while (gpu_task_line.size()) {
			HPEN hPen = CreatePen(PS_SOLID, 1, gpu_task_line[0].color);
			SelectObject(hdc, hPen);

			MoveToEx(hdc, gpu_task_line[0].pointFirst.x, gpu_task_line[0].pointFirst.y, NULL);
			LineTo(hdc, gpu_task_line[0].pointSecond.x, gpu_task_line[0].pointSecond.y);

			gpu_task_line.erase(gpu_task_line.begin());
		}

		while (gpu_task_text.size()) {

			SelectObject(hdc, gpu_task_text[0].font.hFont);

			SIZE size = getSizeText(gpu_task_text[0].text, gpu_task_text[0].font.size);

			RECT rect = { gpu_task_text[0].position.x, gpu_task_text[0].position.y, gpu_task_text[0].position.x + size.cx, gpu_task_text[0].position.y + size.cy };

			SetTextColor(hdc, gpu_task_text[0].font.color);
			DrawText(hdc, (LPCWSTR)gpu_task_text[0].text.c_str(), gpu_task_text[0].text.length(), &rect, DT_TOP | DT_LEFT | DT_SINGLELINE);

			gpu_task_text.erase(gpu_task_text.begin());
		}

		if (Text.isStarted) {
			
			SelectObject(hdc, Text.font.hFont);

			RECT rect = Text.rect;

			HBRUSH hBrush = CreateSolidBrush(RGB(255, 255, 255));
			SelectObject(hdc, hBrush);

			FillRect(hdc, &rect, hBrush);

			SetMapMode(hdc, MM_TEXT);
			SetTextColor(hdc, Text.font.color);
			DrawText(hdc, (LPCWSTR)Text.text.c_str(), Text.text.length(), &rect, DT_TOP | DT_LEFT | DT_SINGLELINE);
		}

		ValidateRect(hWnd, NULL);
		EndPaint(hWnd, &ps);
		break;
    }
	case WM_CHAR: {
		if (Text.isStarted) {
			switch (wParam) {
			case 0x08: { // Back Space
				if (Text.caret.position > 0) {
					Text.caret.position--;
					Text.text.erase(Text.caret.position, 1);
				}
				break;
			}
			case 0x0D: { // Enter
				Text.isStarted = false;
				DestroyCaret();
				return 0;
			}
			default: {
				if (Text.text.length() < Text.max_lenght) {
					Text.text += (TCHAR) wParam;
					Text.caret.position++;
				}
				break;
			}
			}
			HideCaret(hWnd);
			SetCaretPos(WText(Text.caret.position, Text.text, Text.font.size) + Text.rect.left, Text.rect.top);
			InvalidateRect(hWnd, NULL, false);
			ShowCaret(hWnd);
		}
		break;
	}
	case WM_KEYDOWN: {
		if (Text.isStarted) {
			switch (wParam)	{
			case VK_HOME: {
				Text.caret.position = 0;
				break;
			}
			case VK_END: {
				Text.caret.position = Text.text.length();
				break;
			}
			case VK_LEFT: {
				if (Text.caret.position > 0) {
					Text.caret.position--;
				}
				break;
			}
			case VK_RIGHT: {
				if (Text.caret.position < Text.text.length()) {
					Text.caret.position++;
				}
				break;
			}
			case VK_DELETE: {
				Text.text.erase(Text.caret.position, 1);
				break;
			}
			}
			HideCaret(hWnd);
			SetCaretPos(WText(Text.caret.position, Text.text, Text.font.size) + Text.rect.left, Text.rect.top);
			InvalidateRect(hWnd, NULL, false);
			ShowCaret(hWnd);
		}
		break;
	}
	default: {
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
    }
    return 0;
}
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
    UNREFERENCED_PARAMETER(lParam);
    switch (message) {

    case WM_INITDIALOG:
        return (INT_PTR)true;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)true;
        }
        break;
    }
    return (INT_PTR)false;
}